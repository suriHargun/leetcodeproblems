package com.hasu.bullsandcows;

import java.util.ArrayList;

public class ApplicationMain {

	public static void main(String args[]) {

		//Test hardcoded data
		String result = getHint("1243", "4365");
		System.out.println(result);
	}

	public static String getHint(String secret, String guess) {

		String result = null;
		int bullCount = 0;
		int cowCount = 0;

		/*
		 * String.chars() will return ASCII value of each character Post this we
		 * will subtract ASCII value of character with 0's ASCII value, Value
		 * returned will be integer value and mapped to array
		 */

		int[] secretArray = secret.chars().map(x -> x - '0').toArray();
		int[] guessArray = guess.chars().map(x -> x - '0').toArray();

		ArrayList<Integer> secretList = new ArrayList<>();
		ArrayList<Integer> guessList = new ArrayList<>();

		/* Calculating String for Bulls, Secret and Guess element matching with each
		 other and Exist at same location */
		for (int i = 0; i < secretArray.length; i++) {

			/* Checking if element is same and existed at same location then
			 increment count for bull */
			if (secretArray[i] == guessArray[i]) {
				bullCount++;
			} else {
				/* Maintaining List of elements that does not existed at same
				 location when matched */
				secretList.add(secretArray[i]);
				guessList.add(guessArray[i]);
			}
		}

		/* Calculating String for Cows, Guess element exist in Secret Element but
		 do not existed at same location */
		for (Integer guessDigit : guessList) {

			if (secretList.contains(guessDigit)) {
				//Removing element from secret once matched to avoid any duplication overhead
				secretList.remove(guessDigit);
				cowCount++;
			}
		}

		result = bullCount + "" + "A" + cowCount + "B";

		return result;

	}

}
